

val lane_queues = [(1,[])]
val incoming_flights = 1`("Concorde","AIRFRANCE-11YY")
val touch_down_tokens = 1`("Concorde","AIRFRANCE-11YY",1)
val gate_tokens_incoming = 1`("Concorde","AIRFRANCE-11YY",1)
val gates = [FGate(1)]


(*
val lane_queues = [(1,[])]
val incoming_flights = 10`("Concorde","AIRFRANCE-11YY")
val touch_down_tokens = 10`("Concorde","AIRFRANCE-11YY",1)
val gate_tokens_incoming = 10`("Concorde","AIRFRANCE-11YY",1)
val gates = [FGate(1)]
*)

(*
val lane_queues = [(1,[]),(2,[]),(3,[]),(4,[]),(5,[]),(6,[]),(7,[]),(8,[]),(9,[]),(10,[])]
val incoming_flights = 10`("Concorde","AIRFRANCE-11YY")
val gates = [FGate(1),FGate(2),FGate(3),FGate(4),FGate(5),FGate(6),FGate(7),FGate(8),FGate(9),FGate(10)]
val touch_down_tokens = [
	("Concorde","AIRFRANCE-11YY",1),
	("Concorde","AIRFRANCE-11YY",2),
	("Concorde","AIRFRANCE-11YY",3),
	("Concorde","AIRFRANCE-11YY",4),
	("Concorde","AIRFRANCE-11YY",5),
	("Concorde","AIRFRANCE-11YY",6),
	("Concorde","AIRFRANCE-11YY",7),
	("Concorde","AIRFRANCE-11YY",8),
	("Concorde","AIRFRANCE-11YY",9),
	("Concorde","AIRFRANCE-11YY",10)]
val gate_tokens_incoming = [
	("Concorde","AIRFRANCE-11YY",1),
	("Concorde","AIRFRANCE-11YY",2),
	("Concorde","AIRFRANCE-11YY",3),
	("Concorde","AIRFRANCE-11YY",4),
	("Concorde","AIRFRANCE-11YY",5),
	("Concorde","AIRFRANCE-11YY",6),
	("Concorde","AIRFRANCE-11YY",7),
	("Concorde","AIRFRANCE-11YY",8),
	("Concorde","AIRFRANCE-11YY",9),
	("Concorde","AIRFRANCE-11YY",10)
]
*)


val occupied_lanes = lane_queues
val unregistered_flights = ("ADRIA-23XX",[("11c3","Tiiu Kuik"),("12c1","Anu Tali")])

